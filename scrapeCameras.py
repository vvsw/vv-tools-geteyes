# hgc@verivina.com / 2019

#/*
#//     ▄   ▄███▄   █▄▄▄▄ ▄█     ▄   ▄█    ▄   ██   
#//      █  █▀   ▀  █  ▄▀ ██      █  ██     █  █ █  
#// █     █ ██▄▄    █▀▀▌  ██ █     █ ██ ██   █ █▄▄█ 
#//  █    █ █▄   ▄▀ █  █  ▐█  █    █ ▐█ █ █  █ █  █ 
#//   █  █  ▀███▀     █    ▐   █  █   ▐ █  █ █    █ 
#//    █▐            ▀          █▐      █   ██   █  
#//    ▐                        ▐               ▀  
#*/

import concurrent.futures
from time import sleep
from random import randint
import re, json, requests

DEBUG_PRINT=True
MAX_SLEEP_TIME=5
MAX_NUM_THREADS=50
OUTPUT_FILE="scrapedCameras.json"
RE_IP4_ADDRESS=r'http[s]?://(?:[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
RE_MFG_TYPE_HTML=r'href="\/\w+\/bytype\/([a-zA-Z0-9-]*)/'
RE_MAX_PAGES_HTML=r'pagenavigator\(\"\?page=",.(\d*),'

'''
class Camera(Object):
	def __init__(self, ip4_address, get_gif, maker):
		self.ip4_address = ip4_address
		self.get_gif = get_gif
		self.maker = maker
'''

def _dprint(s): 
	if DEBUG_PRINT:
		print(s)

def _assert(c):
	assert(c)

def _http_request(request_str, re_pattern=None, headers=None, sleep_duration=randint(0,MAX_SLEEP_TIME)):
	try:
		sleep(sleep_duration)
		if not headers:
			headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
		ret = requests.get(request_str, headers=headers)
		_dprint("_http_request<%s>='%s'" % (ret.status_code, request_str))
		_assert(ret.status_code == 200)
		if re_pattern != None:
			found = re.findall(re_pattern, str(ret.content))
			_dprint("_htto_request.found=%s, %s" %(len(found), found))
			return found
		return ret.content
	except Exception as e:
		_dprint(e)
		_dprint("_http_request(request_str=%s, re_pattern=%s) failed." % (request_str, str(re_pattern)))

def scrape_pages(max_pages, request_str, re_pattern, data_store, key):
	with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_NUM_THREADS) as executor:
	    future_to_ip_list = {executor.submit(_http_request, request_str + ("?page=%s" % str(page_index)), re_pattern ) : page_index for page_index in range(1,max_pages)}
	    for future in concurrent.futures.as_completed(future_to_ip_list):
	        page_index = future_to_ip_list[future]
	        try:
	            data = future.result()
	        except Exception as exc:
	            print('%r generated an exception: %s' % (page_index, exc))
	        else:
	        	if key not in data_store:
	        		data_store[key] = []
	        	else: data_store[key] += data

def scrape_by_country():
	# scrape by country 
	countries = ['US', 'JP']
	MAX_PAGES=833
	scraped_ip_list = []
	for country in countries:
		scraped_ip_list += scrape_pages(MAX_PAGES, request_str % (country, page_index), re_pattern=RE_IP4_ADDRESS )
	_dprint(scraped_ip_list)

def scrape_by_maker(MAKERS_DICT):
	request_str = "http://www.insecam.org/" 
	# scrape by mfg. maker matching on given pattern
	makers_list = _http_request(request_str=request_str, re_pattern=RE_MFG_TYPE_HTML)
	print(makers_list)
	makers_dict = {}
	scraped_ip_list = []
	with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_NUM_THREADS) as executor:
		request_str="https://www.insecam.org/en/bytype/%s/"
		try:
			future_to_makers_dict = {executor.submit(scrape_pages, int(_http_request(request_str=request_str % maker, re_pattern=RE_MAX_PAGES_HTML)[0]) , request_str % maker, RE_IP4_ADDRESS, MAKERS_DICT, maker ) : maker for maker in makers_list}
			for future in concurrent.futures.as_completed(future_to_makers_dict):
				maker = future_to_makers_dict[future]
				data = future.result()
		except Exception as e:
		    _dprint(e)
		else:
			if key not in data_store:
				data_store[key] = []
			else: 
				MAKERS_DICT[maker] = data


# Execute and write 
with open(OUTPUT_FILE, 'w+') as fp:  
	md = {}
	scrape_by_maker(md)
	json.dump( md, fp, indent=4, sort_keys=True)
